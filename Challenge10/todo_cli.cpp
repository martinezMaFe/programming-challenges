#include <iostream>
#include <string.h>

using namespace std;

int main (void) 
{
    // Selection House-Keeping
    char selection[50];
    char *selectVal;

    // Show Status
    cout << __DATE__ << endl;
    cout << "****************************************************************" << endl;
    cout << "****************Reminder Management System v0.1*****************" << endl;
    cout << "****************************************************************" << endl;

    // Loop Untill Correct Selection is Made
    while(1) { 
        cout << "==>";
        
        // Record Selection
        cin >> selection;
        
        // Convert Selection String to a Numerical Value for Comparison
        selectVal = strtok(selection, "\t");

        // Start Selection of Options
        if(strcmp(selectVal, "help") == 0) {
            // Help Command
            cout << "Available Commands: " << endl;
            cout << "new                : Create new reminder" << endl;
            cout << "list               : List all saved reminders" << endl;
            cout << "edit <List Name>   : Edit contents of <List Name>" << endl;
            cout << "print <List Name>  : Print contents of <List Name>" << endl;
            cout << "exit               : Exit program" << endl;
        }

        // Exit
        else if(strcmp(selectVal, "exit") == 0) {
            cout << "Goodbye!" << endl;
            return 0;
        }

        // Incorrect Command
        else {
            cout << "Invalid Command" << endl;
        }

    }

    return 0;
}